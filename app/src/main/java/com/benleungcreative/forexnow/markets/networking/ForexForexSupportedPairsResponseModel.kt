package com.benleungcreative.forexnow.markets.networking

import com.google.gson.annotations.SerializedName

class ForexForexSupportedPairsResponseModel {

    @SerializedName("code")
    val statusCode: String = ""

    val supportedPairs: ArrayList<String> = ArrayList<String>()

}