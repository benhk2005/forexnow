package com.benleungcreative.forexnow.markets.model

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.os.Message
import com.benleungcreative.forexnow.markets.MarketsConfig
import com.benleungcreative.forexnow.markets.classes.ForexEntry
import com.benleungcreative.forexnow.markets.classes.MarketsData
import com.benleungcreative.forexnow.markets.networking.MarketsAPIManager

class MarketsMainModel {

    var handlerThread: HandlerThread? = null
    var handler: Handler? = null

    val listeners: ArrayList<((MarketsData?) -> Unit)> = ArrayList()
    var activated: Boolean = false

    val forexDataAtFirstSight = HashMap<String, ForexEntry>()
    var availableForexPairs = ArrayList<String>()

    fun requestPriceUpdates(listener: (MarketsData?) -> Unit) {
        listeners.add(listener)
        if (!activated) {
            activateScheduler()
        }
    }

    fun removePriceUpdate(listener: (MarketsData?) -> Unit) {
        if (listeners.indexOf(listener) >= 0) {
            listeners.remove(listener)
        }
        if (listeners.size == 0) {
            deactivateScheduler()
        }
    }

    fun activateScheduler() {
        handler?.removeCallbacksAndMessages(null)
        handlerThread?.quit()
        handlerThread = HandlerThread("UpdateMarketHandlerThread")
        handlerThread?.start()
        handler = Handler(handlerThread!!.looper, UpdateMarketCallback())
        handler?.sendEmptyMessage(0)
        activated = true
    }

    fun deactivateScheduler() {
        handler?.removeCallbacksAndMessages(null)
        handlerThread?.quit()
        activated = false
    }

    fun notifyListeners(marketsData: MarketsData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                listeners.forEach { func ->
                    func.invoke(marketsData)
                }
            }

        })
    }

    fun isFirstSight(forexEntry: ForexEntry, insertIfNotYetSeen: Boolean = true): Boolean {
        if (forexDataAtFirstSight.get(forexEntry.symbol) == null && insertIfNotYetSeen) {
            forexDataAtFirstSight.set(forexEntry.symbol, forexEntry)
            return true
        }
        return false
    }

    inner class UpdateMarketCallback : Handler.Callback {
        override fun handleMessage(p0: Message): Boolean {
            val callback = { marketsData:MarketsData? ->
                marketsData?.forexEntries?.forEach { entry ->
                    if (!isFirstSight(entry)) {
                        entry.updateChangePercentageAndDirection(forexDataAtFirstSight.get(entry.symbol))
                    }
                }
                notifyListeners(marketsData)
                handler?.sendEmptyMessageDelayed(0, MarketsConfig.UPDATE_INTERVAL)
                Unit
            }
            if(availableForexPairs.size > 0){
                getLiveRatesFromForex(availableForexPairs, callback)
            }else {
                MarketsAPIManager.instance.getAllForexPairs { list, t ->
                    list?.let {
                        availableForexPairs = it
                    }
                    getLiveRatesFromForex(availableForexPairs, callback)
                }
            }


            return true
        }

        fun getLiveRatesFromForex(availableForexPairs: ArrayList<String>, callback: (MarketsData?) -> Unit) {
            MarketsAPIManager.instance.getLiveRatesFromForex(availableForexPairs, callback)
        }

    }



}