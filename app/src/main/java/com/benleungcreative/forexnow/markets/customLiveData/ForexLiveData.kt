package com.benleungcreative.forexnow.markets.customLiveData

import androidx.lifecycle.MutableLiveData
import com.benleungcreative.forexnow.markets.classes.MarketsData
import com.benleungcreative.forexnow.markets.model.MarketsMainModel

class ForexLiveData(private val marketsMainModel: MarketsMainModel): MutableLiveData<MarketsData>() {

    override fun onInactive() {
        super.onInactive()
        marketsMainModel.removePriceUpdate(updateCallback)
    }

    var updateCallback = { marketsData:MarketsData? ->
        this.value = marketsData
    }

    override fun onActive() {
        super.onActive()
        marketsMainModel.requestPriceUpdates(updateCallback)
    }
}