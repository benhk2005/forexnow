package com.benleungcreative.forexnow.markets

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.benleungcreative.forexnow.R
import com.benleungcreative.forexnow.databinding.RowForexBinding
import com.benleungcreative.forexnow.markets.classes.ForexEntry
import com.benleungcreative.forexnow.markets.viewHolder.ForexEntryViewHolder

class MarketsForexAdapter : RecyclerView.Adapter<ForexEntryViewHolder>() {

    private var entries: ArrayList<ForexEntry>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForexEntryViewHolder {
        return ForexEntryViewHolder(DataBindingUtil.inflate<RowForexBinding>(LayoutInflater.from(parent.context), R.layout.row_forex, parent,false))
    }

    override fun getItemCount(): Int {
        return entries?.size ?: 0
    }

    override fun onBindViewHolder(holder: ForexEntryViewHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<RowForexBinding>(holder.itemView)
        binding?.model = entries?.get(position)
//        (holder.itemView.context as? FragmentActivity)?.let { activity ->
//            Log.d("activity", "is not null")
//            binding?.viewModel = ViewModelProviders.of(activity, object: ViewModelProvider.Factory{
//                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//                    return ForexEntryViewModel(entries?.get(position)) as T
//                }
//
//            }).get(position.toString(), ForexEntryViewModel::class.java)
//        }

//        binding?.forexEntryModel = entries?.get(position)
        binding?.executePendingBindings()
    }

    fun setEntries(entryViews: ArrayList<ForexEntry>?){
        this.entries = entryViews
        notifyDataSetChanged()
    }

}