package com.benleungcreative.forexnow.markets.classes

enum class PriceDirection {

    FLAT, UPWARD, DOWNWARD

}