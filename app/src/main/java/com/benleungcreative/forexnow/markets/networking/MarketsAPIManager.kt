package com.benleungcreative.forexnow.markets.networking

import android.text.TextUtils
import com.benleungcreative.forexnow.BuildConfig
import com.benleungcreative.forexnow.markets.classes.ForexEntry
import com.benleungcreative.forexnow.markets.classes.MarketsData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

class MarketsAPIManager private constructor() {

    companion object {
        val instance: MarketsAPIManager by lazy {
            MarketsAPIManager()
        }
    }

    private val retrofit: Retrofit
    private val okHttpClient: OkHttpClient
    private val service: ForexAPIInterface

    init {
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
        } else {
            okHttpClient = OkHttpClient.Builder().build()
        }
        retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl("https://www.freeforexapi.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(ForexAPIInterface::class.java)
    }

    fun getAllForexPairs(callback: ((list: ArrayList<String>?, throwable: Throwable?) -> Unit)) {
        service.getPairs().enqueue(object : Callback<ForexForexSupportedPairsResponseModel> {
            override fun onFailure(call: Call<ForexForexSupportedPairsResponseModel>, t: Throwable) {
                callback(null, t)
            }

            override fun onResponse(
                call: Call<ForexForexSupportedPairsResponseModel>,
                response: Response<ForexForexSupportedPairsResponseModel>
            ) {
                if(response.body()?.statusCode == "1001"){
                    callback(response.body()?.supportedPairs, null)
                }else{
                    throw HttpException(response)
                }

            }

        })
    }

    fun getLiveRatesFromForex(currencyPairs: ArrayList<String>, callback: ((MarketsData) -> Unit)) {
        //add system timestamp to _t parameters to avoid cache by CDN
        service.getLiveRates(TextUtils.join(",", currencyPairs), System.currentTimeMillis().toString()).enqueue(object : Callback<ForexResponseModel> {
            override fun onFailure(call: Call<ForexResponseModel>, t: Throwable) {
                MarketsData(false, t, ArrayList())
            }

            override fun onResponse(call: Call<ForexResponseModel>, response: Response<ForexResponseModel>) {
                if (response.body()?.statusCode == "200") {

                    val map = response.body()?.rates
                    if (map == null) {
                        throw IllegalStateException()
                    }
                    val entries = ArrayList<ForexEntry>()
                    currencyPairs.forEach { pair ->
                        entries.add(ForexEntry(pair, map.get(pair)))
                    }
                    callback.invoke(MarketsData(false, null, entries))
                } else {
                    throw HttpException(response)
                }
            }

        })
    }

}