package com.benleungcreative.forexnow.markets.classes

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import com.benleungcreative.forexnow.R
import com.benleungcreative.forexnow.markets.MarketsConfig
import com.benleungcreative.forexnow.markets.helpers.FormattingHelper
import com.benleungcreative.forexnow.markets.helpers.PipsHelper
import com.benleungcreative.forexnow.markets.networking.ForexRateResponseModel
import java.math.BigDecimal
import java.math.RoundingMode


class ForexEntry {

    var symbol: String = "HELLO"
    var source: String = "Forex"
    var direction: PriceDirection = PriceDirection.FLAT
    var changePercentage: BigDecimal = BigDecimal.ZERO
    var centerPrice: BigDecimal = BigDecimal.ZERO
    var sellPrice: BigDecimal = BigDecimal.ZERO
    var buyPrice: BigDecimal = BigDecimal.ZERO
    var equivalentAmount = BigDecimal.ZERO

    constructor() {

    }

    constructor(symbol: String, forexRateResponseModel: ForexRateResponseModel?) {
        this.symbol = FormattingHelper.formatFxPairToSymbol(symbol)
        forexRateResponseModel?.rate?.let { rate ->
            centerPrice = rate
            sellPrice = PipsHelper.addRandomPips(centerPrice)
            buyPrice = PipsHelper.addRandomPips(centerPrice)
        }
    }

    fun getEquivalentAmount(moneyInUsd: BigDecimal): BigDecimal {
        return moneyInUsd.multiply(sellPrice)
    }

    fun updateChangePercentageAndDirection(originalEntry: ForexEntry?) {
        originalEntry?.let { originalEntry ->
            if (!originalEntry.centerPrice.equals(BigDecimal.ZERO)) {
                this.changePercentage = (this.centerPrice.minus(originalEntry.centerPrice))
                    .divide(originalEntry.centerPrice, 10, RoundingMode.HALF_UP)
            }
            when (this.changePercentage.compareTo(BigDecimal.ZERO)) {
                0 -> {
                    this.direction = PriceDirection.FLAT
                }
                -1 -> {
                    this.direction = PriceDirection.DOWNWARD
                }
                1 -> {
                    this.direction = PriceDirection.UPWARD
                }
            }
        }
    }

    fun getColorFromDirection(context: Context): Int {
        return when (direction) {
            PriceDirection.UPWARD -> {
                ResourcesCompat.getColor(context.resources, R.color.direction_upward_color, null)
            }
            PriceDirection.DOWNWARD -> {
                ResourcesCompat.getColor(context.resources, R.color.direction_downward_color, null)
            }
            else -> {
                ResourcesCompat.getColor(context.resources, R.color.direction_flat_color, null)
            }
        }
    }

    fun getDisplayPercentage(bigDecimal: BigDecimal): String {
        return "${MarketsConfig.PERCENT_DECIMAL_FORMAT.format(bigDecimal.multiply(BigDecimal("100")))}%"
    }

    fun getDisplayPrice(bigDecimal: BigDecimal): String {
        return FormattingHelper.formatPrice(bigDecimal)
    }

}