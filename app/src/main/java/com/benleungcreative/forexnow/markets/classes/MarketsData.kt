package com.benleungcreative.forexnow.markets.classes

data class MarketsData(
    val isLoading: Boolean,
    val throwable: Throwable?,
    val forexEntries: ArrayList<ForexEntry>
)
