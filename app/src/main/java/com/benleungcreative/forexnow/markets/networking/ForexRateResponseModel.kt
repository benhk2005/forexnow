package com.benleungcreative.forexnow.markets.networking

import java.math.BigDecimal

class ForexRateResponseModel {


    var rate: BigDecimal? = null
    var timestamp: Long = 0L


}