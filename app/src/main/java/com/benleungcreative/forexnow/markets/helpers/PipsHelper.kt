package com.benleungcreative.forexnow.markets.helpers

import java.math.BigDecimal
import java.util.*

abstract class PipsHelper {

    companion object {

        val onePips = BigDecimal("0.0001")

        fun addPips(price: BigDecimal, pips: Int): BigDecimal {
            if (pips == 0) {
                return price
            } else {
                val addPips = onePips.multiply(BigDecimal(pips))
                return price.add(addPips)
            }
        }

        fun addRandomPips(price: BigDecimal): BigDecimal {
            val random: Random = Random()
            val upOrDown = random.nextBoolean()
            val magnitude = 1+random.nextInt(10)
            return addPips(price, magnitude * (if (upOrDown) -1 else 1))
        }

    }

}