package com.benleungcreative.forexnow.markets.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.benleungcreative.forexnow.databinding.RowForexBinding

class ForexEntryViewHolder:RecyclerView.ViewHolder {

    private val binding: RowForexBinding

    constructor(binding: RowForexBinding) : super(binding.root) {
        this.binding = binding
    }



}