package com.benleungcreative.forexnow.markets.networking

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ForexAPIInterface {

    @GET("live")
    fun getLiveRates(@Query("pairs") paris: String, @Query("_t") timestamp: String): Call<ForexResponseModel>

    @GET("live")
    fun getPairs(): Call<ForexForexSupportedPairsResponseModel>

}