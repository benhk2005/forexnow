package com.benleungcreative.forexnow.markets.helpers

import com.benleungcreative.forexnow.markets.MarketsConfig
import java.math.BigDecimal

abstract class FormattingHelper {

    companion object {

        @JvmStatic
        fun formatFxPairToSymbol(fxPair: String): String {
            if (fxPair.length > 3) {
                return "${fxPair.substring(0, 3)}/${fxPair.substring(3)}"
            } else {
                return fxPair
            }
        }

        @JvmStatic
        fun formatPrice(bigDecimal: BigDecimal): String {
            return MarketsConfig.PRICE_DECIMAL_FORMAT.format(bigDecimal)
        }

    }

}