package com.benleungcreative.forexnow.markets


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.benleungcreative.forexnow.R
import com.benleungcreative.forexnow.databinding.FragmentMarketsBinding
import com.benleungcreative.forexnow.markets.classes.MarketsData
import com.benleungcreative.forexnow.markets.viewModel.MarketsForexViewModel
import com.benleungcreative.forexnow.markets.viewModel.MarketsPortfolioViewModel
import com.benleungcreative.forexnow.markets.viewModel.MarketsViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 *
 */
class MarketsFragment : Fragment() {

    lateinit var binding: FragmentMarketsBinding
    private lateinit var adapter: MarketsForexAdapter
    private lateinit var marketsForexViewModel: MarketsForexViewModel
    private lateinit var portfolioViewModel: MarketsPortfolioViewModel

    companion object {

        val TAG = MarketsFragment::class.java.simpleName

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_markets, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val factory = MarketsViewModelFactory()
        marketsForexViewModel = ViewModelProviders.of(this, factory)[MarketsForexViewModel::class.java]
        portfolioViewModel = ViewModelProviders.of(this, factory)[MarketsPortfolioViewModel::class.java]

        binding.viewModel = marketsForexViewModel
        binding.portfolioViewModel = portfolioViewModel
        adapter = MarketsForexAdapter()

        binding.forexRecyclerView.adapter = adapter
        binding.forexRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.forexRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        marketsForexViewModel.forexLiveData.observe(this, object: Observer<MarketsData>{
            override fun onChanged(t: MarketsData?) {
                adapter.setEntries(t?.forexEntries)
                portfolioViewModel.updateValues(t?.forexEntries)
            }

        })

        marketsForexViewModel.testingField.value = "Hello From AC"
    }

}
