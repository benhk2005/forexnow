package com.benleungcreative.forexnow.markets.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.benleungcreative.forexnow.base.MyApplication
import com.benleungcreative.forexnow.markets.classes.MarketsData
import com.benleungcreative.forexnow.markets.model.MarketsMainModel

class MarketsViewModelFactory : ViewModelProvider.Factory {

    private lateinit var marketsMainModel: MarketsMainModel

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (!(::marketsMainModel.isInitialized)) {
            marketsMainModel = MarketsMainModel()
        }
        when (modelClass) {
            MarketsForexViewModel::class.java -> {
                val vm = MarketsForexViewModel(MyApplication.applicationInstance, marketsMainModel).apply {
                    forexLiveData.value = MarketsData(true, null, ArrayList())
                }
                return vm as T
            }
            MarketsPortfolioViewModel::class.java -> {
                return MarketsPortfolioViewModel(MyApplication.applicationInstance, marketsMainModel) as T
            }
            else -> {
                throw UnsupportedOperationException()
            }
        }
    }


}