package com.benleungcreative.forexnow.markets.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.benleungcreative.forexnow.markets.MarketsConfig
import com.benleungcreative.forexnow.markets.classes.ForexEntry
import com.benleungcreative.forexnow.markets.helpers.FormattingHelper
import com.benleungcreative.forexnow.markets.model.MarketsMainModel
import java.math.BigDecimal
import java.math.RoundingMode

class MarketsPortfolioViewModel(application: Application, private val marketsMainModel: MarketsMainModel) : AndroidViewModel(application) {

    val equity: MutableLiveData<String> = MutableLiveData("-")
    val balance: MutableLiveData<String> = MutableLiveData("-")
    val margin: MutableLiveData<String> = MutableLiveData("(Margin)")
    val used: MutableLiveData<String> = MutableLiveData("(Used)")

    fun updateValues(forexEntries: ArrayList<ForexEntry>?){
        if(forexEntries == null){
            return
        }
        balance.value = FormattingHelper.formatPrice(BigDecimal(forexEntries.size).multiply(BigDecimal("10000")))

        var totalAmount = BigDecimal.ZERO
        forexEntries.forEach { entry ->
            marketsMainModel.forexDataAtFirstSight.get(entry.symbol)?.let { firstSightEntry ->
                if(!entry.buyPrice.equals(BigDecimal.ZERO)) {
                    totalAmount = totalAmount.add(
                        firstSightEntry.getEquivalentAmount(MarketsConfig.DEFAULT_BUY_IN_AMOUNT).divide(
                            entry.buyPrice,
                            10,
                            RoundingMode.HALF_UP
                        )
                    )
                }
            }
        }
        equity.value = FormattingHelper.formatPrice(totalAmount)
//        BigDecimal
    }

}