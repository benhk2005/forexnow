package com.benleungcreative.forexnow.markets

import java.math.BigDecimal
import java.text.DecimalFormat

class MarketsConfig {

    companion object{
        const val UPDATE_INTERVAL = 10000L

        val LIST_OF_CURRENCY_MONITORING = arrayListOf<String>(
            "USDEUR",
            "USDGBP",
            "USDAUD",
            "USDNZD",
            "USDCAD",
            "USDCHF",
            "USDJPY",
            "USDHKD"
        )
        val PERCENT_DECIMAL_FORMAT = DecimalFormat("0.000")
        val PRICE_DECIMAL_FORMAT = DecimalFormat("0.0000")

        val DEFAULT_BUY_IN_AMOUNT = BigDecimal("10000")
    }

}