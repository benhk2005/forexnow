package com.benleungcreative.forexnow.markets.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.benleungcreative.forexnow.markets.customLiveData.ForexLiveData
import com.benleungcreative.forexnow.markets.model.MarketsMainModel

class MarketsForexViewModel(application: Application, private val marketsMainModel: MarketsMainModel): AndroidViewModel(application) {


    var forexLiveData: ForexLiveData = ForexLiveData(marketsMainModel)

    var testingField: MutableLiveData<String> = MutableLiveData("")

}