package com.benleungcreative.forexnow.markets.classes

data class ForexPairEntry(val symbol:String)