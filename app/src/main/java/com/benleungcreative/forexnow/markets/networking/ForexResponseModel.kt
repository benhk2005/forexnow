package com.benleungcreative.forexnow.markets.networking

import com.google.gson.annotations.SerializedName

class ForexResponseModel {

    @SerializedName("code")
    val statusCode: String? = null

    @SerializedName("message")
    val errorMsg: String? = null

    @SerializedName("rates")
    val rates: HashMap<String, ForexRateResponseModel> = HashMap<String, ForexRateResponseModel>()

}