package com.benleungcreative.forexnow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.benleungcreative.forexnow.markets.MarketsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, MarketsFragment(), MarketsFragment.TAG).commit()
    }
}
