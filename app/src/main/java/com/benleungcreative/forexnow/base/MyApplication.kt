package com.benleungcreative.forexnow.base

import android.app.Application

class MyApplication : Application() {

    companion object{
        lateinit var applicationInstance: Application
    }

    override fun onCreate() {
        super.onCreate()
        applicationInstance = this
    }
}