package com.benleungcreative.forexnow

import com.benleungcreative.forexnow.markets.helpers.PipsHelper
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal

class PipsHelperTest {

    val originalValue:BigDecimal = BigDecimal("1.2322")

    @Test
    fun pipsHelper_noAddPips(){
        assertTrue(PipsHelper.addPips(originalValue, 0).equals(originalValue))
    }

    @Test
    fun pipsHelper_add1Pips(){
        assertTrue(PipsHelper.addPips(originalValue, 1).equals(BigDecimal("1.2323")))
    }

    @Test
    fun pipsHelper_add6Pips(){
        assertTrue(PipsHelper.addPips(originalValue, 6).equals(BigDecimal("1.2328")))
    }

    @Test
    fun pipsHelper_add12Pips(){
        assertTrue(PipsHelper.addPips(originalValue, 12).equals(BigDecimal("1.2334")))
    }

    @Test
    fun pipsHelper_minus1Pips(){
        assertTrue(PipsHelper.addPips(originalValue, -1).equals(BigDecimal("1.2321")))
    }

    @Test
    fun pipsHelper_minus6Pips(){
        assertTrue(PipsHelper.addPips(originalValue, -6).equals(BigDecimal("1.2316")))
    }

    @Test
    fun pipsHelper_minus12Pips(){
        assertTrue(PipsHelper.addPips(originalValue, -12).equals(BigDecimal("1.2310")))
    }

    @Test
    fun pipsHelper_testRandomPipsRange(){
        var pass = true
        for (i in 0 until 10000){
            val lowerBounds = PipsHelper.addPips(originalValue, -11)
            val upperBounds = PipsHelper.addPips(originalValue, 11)
            val newPrice = PipsHelper.addRandomPips(originalValue)
            pass = pass and (lowerBounds.compareTo(newPrice) == -1 && upperBounds.compareTo(newPrice) == 1)
        }
        assertTrue(pass)
    }

}