package com.benleungcreative.forexnow

import com.benleungcreative.forexnow.markets.helpers.FormattingHelper
import org.junit.Assert.assertEquals
import org.junit.Test

class FormattingUnitTest {

    @Test
    fun formattingTest_fxPairToDisplaySymbol1Character(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("A"), "A")
    }

    @Test
    fun formattingTest_fxPairToDisplaySymbol2Characters(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("AB"), "AB")
    }

    @Test
    fun formattingTest_fxPairToDisplaySymbol3Characters(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("ABC"), "ABC")
    }

    @Test
    fun formattingTest_fxPairToDisplaySymbol4Characters(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("ABCD"), "ABC/D")
    }

    @Test
    fun formattingTest_fxPairToDisplaySymbol5Characters(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("ABCDE"), "ABC/DE")
    }

    @Test
    fun formattingTest_fxPairToDisplaySymbol6Characters(){
        assertEquals(FormattingHelper.formatFxPairToSymbol("ABCDEF"), "ABC/DEF")
    }

}